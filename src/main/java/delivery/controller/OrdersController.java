package delivery.controller;

import delivery.dao.OrderDAO;
import delivery.dao.CustomerDAO;
import delivery.dao.StatusHistoryDAO;
import delivery.model.Order;
import delivery.model.Status;
import delivery.model.StatusHistory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/orders")
public class OrdersController
{
    @Autowired
    OrderDAO orderDAO;

    @Autowired
    StatusHistoryDAO historyDao;

    @Autowired
    CustomerDAO customerDAO;

    /**
     * При переходе по url /orders вызывается index.html
     * @return объект {@link ModelAndView}
     */
    @GetMapping()
    public ModelAndView getIndex()
    {
        ModelAndView model = new ModelAndView();
        model.setViewName("index");
        return model;
    }

    /**
     * При переходе по url /orders/all вызывается all.html
     * @return объект {@link ModelAndView}
     */
    @GetMapping(value = "/all")
    public ModelAndView getOrderList()
    {
        ModelAndView model = new ModelAndView();
        model.addObject("orders", orderDAO.getAll());
        model.setViewName("all");
        return model;
    }

    /**
     * При получении GET запроса с параметром id, метод должен вернуть объект {@link ModelAndView}
     * @param id_order - id заказа в БД
     * @return - страница детализации заказа
     */
    @GetMapping(value="/{id}")
    public @ResponseBody
    ModelAndView getOrder(@PathVariable("id") Optional<String> id_order)
    {
        try
        {
            ModelAndView modelView = new ModelAndView();
            if (!id_order.isPresent())
            {
                modelView.setStatus(HttpStatus.NOT_FOUND);
                return modelView;
            }
            long id = Long.parseLong(id_order.get());

            Optional<Order> order = orderDAO.get(id);

            List<StatusHistory> history = historyDao.getAllByOrder(id);

            if(order.isPresent())
            {
                modelView.setStatus(HttpStatus.OK);
                modelView.setViewName("orderDetails");
                modelView.addObject("order", order.get());
                modelView.addObject("statusHistory", history);
                modelView.addObject("statusList", Status.getCodeDescription());
            }
            return modelView;
        }
        catch (NumberFormatException ex)
        {
            throw new NumberFormatException();
        }
    }

    /**
     *
     * @return объект {@link ModelAndView} - страница с формой создания заказа
     */
    @GetMapping(value = "/createOrder")
    public ModelAndView createOrderForm()
    {
        ModelAndView model = new ModelAndView();
        model.addObject("customers", customerDAO.getAll());
        model.addObject("order", new Order());
        model.addObject("statusList", Status.getCodeDescription());
        model.setViewName("createOrder");
        return model;
    }


    /**
     *
     * @param order - объект {@link Order} с данными формы для ввода данных нового заказа
     * @return объект {@link ModelAndView}
     */
    @PostMapping(value="/createOrder")
    public ModelAndView createOrder(@ModelAttribute("order")Order order)
    {
        ModelAndView model = new ModelAndView();
        System.out.println("/createOrder " + order + " " + order.getCustomer());
        Optional<Long> id = orderDAO.save(order);

        if (id.isPresent())
        {
            model.addObject("id", id.get());
            model.setViewName("redirect:{id}");
            return model;
        }

        model.addObject("error", "Не удалось добавить заказ.");
        model.setViewName("createOrder");
        return model;
    }

    /**
     *
     * @param order - объект {@link Order} с данными заказа
     * @return объект {@link ModelAndView}
     */
    @PostMapping(value="/editStatus")
    public ModelAndView updateOrderStatus(@ModelAttribute("order")Order order)
    {
        orderDAO.update(order);
        historyDao.save(order);
        ModelAndView model = new ModelAndView();
        model.addObject("id", order.getId());
        model.setViewName("redirect:{id}");
        return model;
    }


}
