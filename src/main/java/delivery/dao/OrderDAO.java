package delivery.dao;


import delivery.mapper.OrderRowMapper;
import delivery.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;

/**
 * Обработка запросов к БД по таблице Orders
 */
public class OrderDAO implements Dao<Order>
{
    private static final String GET = "SELECT ID_ORDER, PHONE, CREATED_TIME, LAST_MODIFIED_TIME, STATUS, ORDER_SUM FROM ORDERS WHERE ID_ORDER = ?";
    private static final String GET_ALL = "SELECT ID_ORDER, PHONE, CREATED_TIME, LAST_MODIFIED_TIME, STATUS, ORDER_SUM FROM ORDERS";
    private static final String SAVE = "INSERT INTO ORDERS\n" +
                                        "(phone, status, created_time, last_modified_time, order_sum)\n" +
                                        "VALUES(?, ?, datetime('now', 'localtime'), datetime('now', 'localtime'), ?);";
    private static final String UPDATE = "UPDATE ORDERS\n" +
                                        "SET status=?, last_modified_time=datetime('now', 'localtime')" +
                                        "WHERE id_order=?;";

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public OrderDAO(JdbcTemplate template)
    {
        this.jdbcTemplate = template;
    }

    @Override
    public Optional<Order> get(long id)
    {
        if (Long.valueOf(id) == null)
        {
            return Optional.empty();
        }
        List<Order> orders = jdbcTemplate.query
                (
                    GET
                    , ps -> ps.setLong(1, id)
                    , new OrderRowMapper()
                );

        Optional<Order> order = orders.stream().filter(x-> x.getId() == id).findFirst();

        return order;
    }

    @Override
    public List<Order> getAll()
    {
        List<Order> orders = null;
        orders = jdbcTemplate.query(GET_ALL, new OrderRowMapper());
        return orders;
    }

    @Override
    public Optional<Long> save(Order order)
    {
        Long id = jdbcTemplate.execute(connection ->
                {
                    PreparedStatement ps =
                            connection.prepareStatement(SAVE, Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, order.getCustomer().getPhone());
                    ps.setLong(2, order.getStatus().getCode());
                    ps.setDouble(3, order.getOrderSum());
                    return ps;
                }, (PreparedStatementCallback<Long>) ps ->
                {
                    ps.executeUpdate();

                    ResultSet result = ps.getGeneratedKeys();

                    long key = 0;

                    while (result.next()) {
                        key = result.getLong(1);
                    }

                    return key;

                }
        );
        return Optional.of(id);
    }


    @Override
    public Optional<Order> update(Order order)
    {
        int affectedRows = jdbcTemplate.update(UPDATE, order.getStatus().getCode(), order.getId());

        if (affectedRows == 1)
        {
            return this.get(order.getId());
        }
        return Optional.empty();
    }

}
