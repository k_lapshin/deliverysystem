package delivery.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface Dao<T> {

    /**
     * Запрашивает запись по указанному id.
     * @param id - уникальный идентификатор записи
     * @return объект класса {@link Optional <T>}
     * @throws SQLException
     * @throws IOException
     */
    Optional<T> get(long id);

    /**
     * Запрашивает все записи
     * @return - список записей типа T
     * @throws IOException
     */
    List<T> getAll() throws IOException, SQLException;

    /**
     * Сохраняет объект, создавая новую запись
     * @param t - объект. который нужно сохранить
     * @throws SQLException
     * @throws IOException
     * @return - id вставленной записи
     */
    Optional<Long> save(T t) throws SQLException;

    /**
     * Изменяет запись с @param id
     * @param t - объект, содержащий измененные поля
     * @throws SQLException
     * @throws IOException
     */
    Optional<T> update(T t) throws SQLException;

}