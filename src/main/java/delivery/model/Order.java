package delivery.model;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;


public class Order
{
    private long id;
    private Customer customer;
    private Status status;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime created;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lastModified;
    //TODO заменить на BigDecimal. Лучше вообще заменить на Item<List>
    private double orderSum;

    public Order()
    {
    }

    public Order(long id, Customer customer, Status status, LocalDateTime created, LocalDateTime lastModified, double orderSum) {
        this.id = id;
        this.customer = customer;
        this.status = status;
        this.created = created;
        this.lastModified = lastModified;
        this.orderSum = orderSum;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public void setCustomer(Customer customer)
    {
        this.customer = customer;
    }

    public void setStatus(Status status)
    {
        this.status = status;
    }

    public void setCreated(LocalDateTime created)
    {
        this.created = created;
    }

    public void setLastModified(LocalDateTime lastModified)
    {
        this.lastModified = lastModified;
    }

    public void setOrderSum(double orderSum)
    {
        this.orderSum = orderSum;
    }

    public long getId()
    {
        return id;
    }

    public Status getStatus()
    {
        return status;
    }

    public double getOrderSum()
    {
        return orderSum;
    }

    public LocalDateTime getLastModified()
    {
        return lastModified;
    }

    public LocalDateTime getCreated()
    {
        return created;
    }

    public Customer getCustomer()
    {
        return customer;
    }

    @Override
    public String toString()
    {
        return "Заказ № "
                + getId()
                + " на сумму "
                + getOrderSum()
                + ". "
                + getStatus().getDescription();
    }

    @Override
    public boolean equals(Object o)
    {
        if(this == o)
        {
            return true;
        }
        if (o == null || o.getClass() != this.getClass())
        {
            return false;
        }

        Order order = (Order) o;

        return this.id == order.id
            && this.customer.equals(order.customer)
            && this.created.equals(order.created);
    }
}
