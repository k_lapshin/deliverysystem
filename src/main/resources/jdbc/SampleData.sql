INSERT INTO CUSTOMER (phone) VALUES (89895662323), (89895662343);

INSERT INTO STATUS (code) VALUES (1),(2),(3),(4),(5);

INSERT INTO ORDERS (phone, status, created_time, last_modified_time, order_sum)
VALUES (89895662323, 1, '2022-02-21 12:32:29', '2022-02-21 12:32:29', 450.36);
INSERT INTO ORDERS (phone, status, created_time, last_modified_time, order_sum)
VALUES (89895662323, 3, '2022-02-21 13:45:29', '2022-02-21 15:31:40', 600);

INSERT INTO STATUS_HISTORY (id_order, status, date_set)
VALUES(1, 1, '2022-02-21 12:32:29');
INSERT INTO STATUS_HISTORY (id_order, status, date_set)
VALUES(1, 2, '2022-02-21 12:45:25');

