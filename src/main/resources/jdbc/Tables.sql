CREATE table CUSTOMER (
	phone VARCHAR(255) PRIMARY KEY
);

CREATE table STATUS (

	code integer PRIMARY KEY  NOT NULL
	
);

CREATE table ORDERS (
	id_order integer AUTO_INCREMENT  PRIMARY KEY,
	phone VARCHAR(255) NOT NULL,
	status integer  NOT NULL,
	created_time text  NOT NULL,
	last_modified_time text  NOT NULL,
	order_sum real NOT NULL,
	FOREIGN KEY(phone) REFERENCES CUSTOMER(phone),
	FOREIGN KEY (status) REFERENCES STATUS(code)
);

CREATE TABLE "STATUS_HISTORY" (
	id_status_history integer  AUTO_INCREMENT  PRIMARY KEY,
	id_order INTEGER NOT NULL,
	status INTEGER NOT NULL, date_set TEXT NOT NULL,
	CONSTRAINT STATUS_HISTORY_PK PRIMARY KEY (id_status_history),
	CONSTRAINT STATUS_HISTORY_FK FOREIGN KEY (id_order) REFERENCES ORDERS(id_order),
	CONSTRAINT STATUS_HISTORY_FK_1 FOREIGN KEY (status) REFERENCES STATUS(code)
);